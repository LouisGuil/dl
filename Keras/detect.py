# detect passagepieton in photos with mask rcnn model
from os import listdir
from xml.etree import ElementTree
from numpy import zeros
from numpy import asarray
from numpy import expand_dims
from mrcnn.config import Config
from mrcnn.model import MaskRCNN
from mrcnn.model import mold_image
from mrcnn.utils import Dataset
import numpy as np
from osgeo import gdal
from osgeo import osr
import os
from PIL import Image
import glob
from pathlib import Path
import csv


def decoupimage():
	# Répertoir de l'image à découper
	in_path = 'imgtif/'
	# Nom de l'image à découper  
	input_dir = glob.glob('imgtif/*.tif')[0]
  	input_filename = input_dir.split('/')[1]
	output_filename = 'tile_'

	# Taille des tuiles à découper
	tile_size_x = 435
	tile_size_y = 450

	ds = gdal.Open(in_path + input_filename)
	band = ds.GetRasterBand(1)
	xsize = band.XSize
	ysize = band.YSize

	dirfin = "tmp_til_jpg/"
	dirtmp1 = "tmp_til_tif/"

	# Create temporary directory
	os.system("mkdir " + dirtmp1)
	os.system("mkdir " + dirfin)
	for i in range(0, xsize, tile_size_x):
			for j in range(0, ysize, tile_size_y):
					# DECOUPAGE D'UNE IMAGE EN TUILE
					com_string = "gdal_translate -of GTIFF -srcwin " + str(i)+ ", " + str(j) + ", " + str(tile_size_x) + ", " + str(tile_size_y) + " " + str(in_path) + str(input_filename) + " " + str(dirtmp1) + str(output_filename) + str(i) + "_" + str(j) + ".tif"
					os.system(com_string)

					# TIF TO JPEG
					com_string3 = "gdal_translate -of JPEG -co 'WORLDFILE=YES' " + str(dirtmp1) + str(output_filename) + str(i) + "_" + str(j) + ".tif" + " " + str(dirfin) + str(output_filename) + str(i) + "_" + str(j) + ".jpg"
					os.system(com_string3)

#decoupimage()

class passagepietonDatasetDetection(Dataset):
	# load the dataset definitions
	def load_dataset(self, dataset_dir):
		# define one class
		self.add_class("dataset", 1, "passagepieton")
		# define data locations
		images_dir = dataset_dir
		# find all images
		for filename in listdir(images_dir):
			# extract image id
			image_id = filename[:-4]
			img_path = images_dir + filename
			# add to dataset
			self.add_image('dataset', image_id=image_id, path=img_path)
	
	# info on image
	def image_name(self, name_img):
		info = self.image_info[name_img]
		return info


# define the prediction configuration
class PredictionConfig(Config):
	# define the name of the configuration
	NAME = "passagepieton_cfg"
	# number of classes (background + passagepieton)
	NUM_CLASSES = 1 + 1
	# simplify GPU config
	GPU_COUNT = 1
	IMAGES_PER_GPU = 1

#Prediction on new dataset
def predictionNewData(dataset, models, cfg):
	"""
	os.system("touch coordPPfinal.txt")
	f = open("coordPPfinal.txt", "w")
	f.write("ID latitude longitude" + '\n')
	"""

	my_file = Path("coordPassagePieton.csv")
	if my_file.is_file():
		os.system('rm coordPassagePieton.csv')

	os.system("touch coordPassagePieton.csv")

	with open('coordPassagePieton.csv', mode='a') as coordPassage:
		coordPassage = csv.writer(coordPassage, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
		coordPassage.writerow(['ID', 'longitude', 'latitude'])

	Idpassage = 0
	for i in range(800):
		# load the image
		image = dataset.load_image(i)
		#print(dataset.image_name(i)['id']+".tif")

		# convert pixel values (e.g. center)
		scaled_image = mold_image(image, cfg)
		# convert image into one sample
		sample = expand_dims(scaled_image, 0)
		# make prediction
		yhat = model.detect(sample, verbose=0)[0]
		

		#print(dataset.image_name(i)['id'])
		#print("tmp_til_tif/" + dataset.image_name(i)['id'] + ".tif")
		for box in yhat['rois']:
			Idpassage += 1
			# get coordinates
			y1, x1, y2, x2 = box
			# calculate width and height of the box
			
			#width, height = x2 - x1, y2 - y1
			#print(x1,y1,x2,y2)
			larg = x2-x1
			long = y2-y1

			larg2 = larg/2
			long2 = long/2

			xpix = x1 + larg2
			ypix = y1 +	long2
			latitude, longitude = getLatLng("tmp_til_tif/" + dataset.image_name(i)['id'] + ".tif", xpix, ypix)
			#print("pixel : " + str(xpix) + " " + str(ypix))
			#print("geographique : " + str(latitude) + " " + str(longitude))
			coordPassage.writerow(['ID',longitude, latitude])
			#f.write(str(Idpassage) + " " + str(latitude)+ " " +str(longitude)+'\n')
	#f.close

	# Delete temporary directory
	#os.system("rm -r til_tif")
	#os.system("rm -r til_jpg")

def getLatLng(img_til, px, py):
    # open the dataset and get the geo transform matrix
    ds = gdal.Open(img_til) 
    xoffset, px_w, rot1, yoffset, rot2, px_h = ds.GetGeoTransform()

    # supposing x and y are your pixel coordinate  
    # this is how to get the coordinate in space.
    x = px
    y = py
    posX = px_w * x + rot1 * y + xoffset
    posY = rot2 * x + px_h * y + yoffset

    # shift to the center of the pixel
    posX += px_w / 2.0
    posY += px_h / 2.0

    return posX, posY

# create config
cfg = PredictionConfig()
# define the model
model = MaskRCNN(mode='inference', model_dir='./', config=cfg)
# load model weights
model_path = 'ApprentissageJeu435x450/graphtest9_50e131s1bs101r/mask_rcnn_passagepieton_cfg_0050.h5'
model.load_weights(model_path, by_name=True)
# Load and prepare dataset of detection
test_set = passagepietonDatasetDetection()
test_set.load_dataset('tmp_til_jpg/')
test_set.prepare()

predictionNewData(test_set, model, cfg)
