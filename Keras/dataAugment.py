import imageio
import imgaug as ia
from imgaug.augmentables.bbs import BoundingBox, BoundingBoxesOnImage
from imgaug import augmenters as iaa 
import os
from os import listdir
import cv2
import xml.etree.ElementTree as ET

ia.seed(1)

images_dir = "passagepieton3/images/"
annots_dir = "passagepieton3/annots/"

def augImgRotate(rotation, numI):
    for filename in listdir(images_dir):
        
        image = imageio.imread(images_dir + filename)
        nomXml = filename.split('.')
        tree = ET.parse(annots_dir + nomXml[0] + ".xml")
        root = tree.getroot()

        boxes = list()
        for box in root.findall('.//bndbox'):
            xmin = int(box.find('xmin').text)
            ymin = int(box.find('ymin').text)
            xmax = int(box.find('xmax').text)
            ymax = int(box.find('ymax').text)
            coors = [xmin, ymin, xmax, ymax]
            boxes.append(coors)

        boxesAug = []
        for i in range(len(boxes)):
            bbs = BoundingBoxesOnImage([
                BoundingBox(x1=boxes[i][0], x2=boxes[i][2], y1=boxes[i][1], y2=boxes[i][3])
            ], shape=image.shape)

            image_aug, bbs_aug = iaa.Affine(rotate=rotation)(image=image, bounding_boxes=bbs)
            coorsAug = [bbs_aug.bounding_boxes[0].x1, bbs_aug.bounding_boxes[0].y1, bbs_aug.bounding_boxes[0].x2, bbs_aug.bounding_boxes[0].y2]
            boxesAug.append(coorsAug)

        tree = ET.parse(annots_dir + nomXml[0] + ".xml")
        root = tree.getroot()  
        j = 0

        for box in root.findall('.//bndbox'):
            box.find('xmin').text = str(boxesAug[j][0])
            box.find('ymin').text = str(boxesAug[j][1])
            box.find('xmax').text = str(boxesAug[j][2])
            box.find('ymax').text = str(boxesAug[j][3])
            j += 1

        imageio.imwrite(images_dir + str(numI) + '.jpg', image_aug)
        tree.write(annots_dir + str(numI) + ".xml")
        numI += 1

    return numI


def augImgColor():

    return 

# Voir ce qui marche pas avec l'enregistrement de l'image
# Comprendre pourquoi les noms des images et xml ne se suivent pas 
# Savoir si ça a un effet lors de la création des jeux de données lors de l'apprentissage
# Calculer le nombre d'image pour remplire automatiquement le numéro de l'image
toto = augImgRotate(90,951)
print(toto)
#augImg(270,668)