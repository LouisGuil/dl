# détection automatique de passage piéton sur image aériennes

Stage à l'ENSG/IGN pour la détection automatique de passage piéton sur image aériennes.

# INSTALLATION KERAS MASK-RCNN 

## INSTALLATION D'ANACONDA 

```bash
wget https://repo.anaconda.com/archive/Anaconda3-2018.12-Linux-x86_64.sh
bash Anaconda3-2018.12-Linux-x86_64.sh(pas ensudo!)

conda update --all

conda create -n dl pip python=3.6

conda activate dl

conda install -c conda-forge opencv scikit-image fiona keras-gpu

## INSTALLATION DE LA LIB MASK-RCNN 

```bash
git clone https://github.com/matterport/Mask_RCNN.git
cd Mask_RCNN

~/anaconda3/envs/dl/bin/python setup.py install
```

## INSTALLATION DE cuDNN et CUDA en version !9!

https://developer.nvidia.com/cuda-90-download-archive?
https://docs.nvidia.com/deeplearning/sdk/cudnn-install/index.html

## APPRENTISSAGE 

```python
python learn.py
```

### SUIVIS DE L'APPRENTISSAGE 

```python
tensorboard --logdir=path/to/log-directory
```

## EVALUATION DE L'APPRENTISSAGE 

Renseigne l'IoU du modèle

```python
python eval_model_on_dataset.py
```
## DETECTION 
Mettre l'image dans le dossier imgtif puis lancer la détection avec :

```python
python detect.py
```

## IMAGE AUGMENTATION

Si vous ne disposez de pas assez d'image ou si vôtre modèle a du mal à généraliser la détéction il est possible d'en créer de nouvelle artificiellement.
En effet on peut prendre une image puis en la faisant tourner en sortir une nouvelle ou encore en changeant la radiométrie de l'image.
Pour cela j'ai réalisé un script qui permet de faire une rotation des images dans le sens que vous voulez ainsi que le fichier xml qui indique les bounding box.

```python
python dataAugment.py
```

Il faut au préalable mettre toutes les images dans un dossier "/images" et tous les fichiers xml dans un dossier "/annots"

# INSTALLATION TENSORFLOW OBJECT DETECTION

https://tensorflow-object-detection-api-tutorial.readthedocs.io/en/latest/install.html#tensorflow-models-installation

OU

https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/installation.md

Cette méthode de détection a été utilisé uniquement sur CPU.

```python
conda create -n tf pip python=3.6
conda activate tf
conda install tensorflow
conda install protobuf-compiler python-pil python-lxml python-tk
conda install pillow lxml jupyter matplotlib opencv cython fiona
```


## PREPARATION DE LA DONNEE
### GENERAT TFRECORD 

```python
python xml_to_csv.py
python generate_tfrecord.py --csv_input=data/test_labels.csv --output_path=data/test.record --image_dir=images/test/
python generate_tfrecord.py --csv_input=data/train_labels.csv --output_path=data/train.record --image_dir=images/train/
```

## LANCEMENT APPRENTISSAGE 
```bash
cd TensorFlow/models/research
export PYTHONPATH=$PYTHONPATH:`pwd`:`pwd`/slim

conda activate <nom de lenvironnement>

cd object_detection
```

```python
python train.py --logtostderr --train_dir=training/ --pipeline_config_path=training/ssd_mobilenet_v1_coco_2017_11_17.config
```
## SUIVIS DE L'APPRENTISSAGE
```python
--logdir=training/
```

## EXPORT DU MODELE
```bash
export PYTHONPATH=$PYTHONPATH:`pwd`:`pwd`/slim
```

```python
python export_inference_graph.py \
    --input_type image_tensor \
    --pipeline_config_path training/ssd_mobilenet_v1_coco_2017_11_17.config \
    --trained_checkpoint_prefix training/model.ckpt-5485 \
    --output_directory passagepieton_graph
```

## DETECTION
Placez le dossier ssd_mobilenet_v1_coco_2017_11_17 dans TensorFlow/models/research/object_detection/ ainsi que tous les fichiers/dossiers contenue dans le dossier "Détection" 
Mettre l'image dans le dossier predic_passagepieton puis lancer la détection avec :

```python
python detection.py
```
