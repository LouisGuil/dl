import numpy as np
import os
import six.moves.urllib as urllib
import sys
import tarfile
import tensorflow as tf
import zipfile
import os
import csv

from distutils.version import StrictVersion
from collections import defaultdict
from io import StringIO
from matplotlib import pyplot as plt
from PIL import Image
from pathlib import Path
from utils import label_map_util
from utils import visualization_utils as vis_util
from osgeo import gdal
from osgeo import osr

# This is needed since the notebook is stored in the object_detection folder.
sys.path.append("..")
from object_detection.utils import ops as utils_ops

if StrictVersion(tf.__version__) < StrictVersion('1.12.0'):
  raise ImportError('Please upgrade your TensorFlow installation to v1.12.*.')

# What model to download.
MODEL_NAME = 'passagepieton_graph'
# Path to frozen detection graph. This is the actual model that is used for the object detection.
PATH_TO_FROZEN_GRAPH = MODEL_NAME + '/frozen_inference_graph.pb'
# List of the strings that is used to add correct label for each box.
PATH_TO_LABELS = os.path.join('training', 'Detection-passage.pbtxt')
NUM_CLASSES = 1

detection_graph = tf.Graph()
with detection_graph.as_default():
  od_graph_def = tf.GraphDef()
  with tf.gfile.GFile(PATH_TO_FROZEN_GRAPH, 'rb') as fid:
    serialized_graph = fid.read()
    od_graph_def.ParseFromString(serialized_graph)
    tf.import_graph_def(od_graph_def, name='')

category_index = label_map_util.create_category_index_from_labelmap(PATH_TO_LABELS, use_display_name=True)

def load_image_into_numpy_array(image):
  (im_width, im_height) = image.size
  return np.array(image.getdata()).reshape(
      (im_height, im_width, 3)).astype(np.uint8)

def run_inference_for_single_image(image, graph):
  with graph.as_default():
    with tf.Session() as sess:
      # Get handles to input and output tensors
      ops = tf.get_default_graph().get_operations()
      all_tensor_names = {output.name for op in ops for output in op.outputs}
      tensor_dict = {}
      for key in [
          'num_detections', 'detection_boxes', 'detection_scores',
          'detection_classes', 'detection_masks'
      ]:
        tensor_name = key + ':0'
        if tensor_name in all_tensor_names:
          tensor_dict[key] = tf.get_default_graph().get_tensor_by_name(
              tensor_name)
      if 'detection_masks' in tensor_dict:
        # The following processing is only for single image
        detection_boxes = tf.squeeze(tensor_dict['detection_boxes'], [0])
        detection_masks = tf.squeeze(tensor_dict['detection_masks'], [0])
        # Reframe is required to translate mask from box coordinates to image coordinates and fit the image size.
        real_num_detection = tf.cast(tensor_dict['num_detections'][0], tf.int32)
        detection_boxes = tf.slice(detection_boxes, [0, 0], [real_num_detection, -1])
        detection_masks = tf.slice(detection_masks, [0, 0, 0], [real_num_detection, -1, -1])
        detection_masks_reframed = utils_ops.reframe_box_masks_to_image_masks(
            detection_masks, detection_boxes, image.shape[1], image.shape[2])
        detection_masks_reframed = tf.cast(
            tf.greater(detection_masks_reframed, 0.5), tf.uint8)
        # Follow the convention by adding back the batch dimension
        tensor_dict['detection_masks'] = tf.expand_dims(
            detection_masks_reframed, 0)
      image_tensor = tf.get_default_graph().get_tensor_by_name('image_tensor:0')

      # Run inference
      output_dict = sess.run(tensor_dict,
                             feed_dict={image_tensor: image})

      # all outputs are float32 numpy arrays, so convert types as appropriate
      output_dict['num_detections'] = int(output_dict['num_detections'][0])
      output_dict['detection_classes'] = output_dict[
          'detection_classes'][0].astype(np.int64)
      output_dict['detection_boxes'] = output_dict['detection_boxes'][0]
      output_dict['detection_scores'] = output_dict['detection_scores'][0]
      if 'detection_masks' in output_dict:
        output_dict['detection_masks'] = output_dict['detection_masks'][0]
  return output_dict




#####################################################################
################## DECOUPE DE L'IMAGE ###############################
#####################################################################
def decoupimage():
  # Répertoir de l'image à découper
  in_path = 'predic_passagepieton/'
  # Nom de l'image à découper  
  input_dir = glob.glob('/predic_passagepieton/*.tif')[0]
  input_filename = input_dir.split('/')[1]

  # Taille des tuiles à découper
  tile_size_x = 435
  tile_size_y = 450

  ds = gdal.Open(in_path + input_filename)
  band = ds.GetRasterBand(1)
  xsize = band.XSize
  ysize = band.YSize

  dirfin = "predic_passagepieton/tmp_til_jpg/"
  dirtmp1 = "predic_passagepieton/tmp_til_tif/"

  # Create temporary directory
  os.system("mkdir " + dirtmp1)
  os.system("mkdir " + dirfin)

  w = 1

  for i in range(0, xsize, tile_size_x):
    for j in range(0, ysize, tile_size_y):
      # DECOUPAGE D'UNE IMAGE EN TUILE
      com_string = "gdal_translate -of GTIFF -srcwin " + str(i)+ ", " + str(j) + ", " + str(tile_size_x) + ", " + str(tile_size_y) + " " + str(in_path) + str(input_filename) + " " + dirtmp1 + str(w) + ".tif"
      os.system(com_string)

      # TIF TO JPEG
      com_string3 = "gdal_translate -of JPEG -co 'WORLDFILE=YES' " + dirtmp1 + str(w) + ".tif" + " " + dirfin + str(w) + ".jpg"
      os.system(com_string3)
      w += 1

  return w

nbrImage = decoupimage() - 1

# If you want to test the code with your images, just add path to the images to the TEST_IMAGE_PATHS.
PATH_TO_TEST_IMAGES_DIR = 'predic_passagepieton/tmp_til_jpg/'
TEST_IMAGE_PATHS = [ os.path.join(PATH_TO_TEST_IMAGES_DIR, '{}.jpg'.format(i)) for i in range(1, nbrImage) ]
# Size, in inches, of the output images.
######IMAGE_SIZE = (12, 8)

#####################################################################
############ RECUPERATION DES COORDONNEES GEOGRAPHIQUES #############
#####################################################################
def getLatLng(img_til, px, py):
    # open the dataset and get the geo transform matrix
    ds = gdal.Open(img_til) 
    xoffset, px_w, rot1, yoffset, rot2, px_h = ds.GetGeoTransform()

    # supposing x and y are your pixel coordinate  
    # this is how to get the coordinate in space.
    x = px
    y = py
    posX = px_w * x + rot1 * y + xoffset
    posY = rot2 * x + px_h * y + yoffset

    # shift to the center of the pixel
    posX += px_w / 2.0
    posY += px_h / 2.0

    return posX, posY


#####################################################################
########################### DETECTION ###############################
#####################################################################

my_file = Path("predic_passagepieton/Tensorflow_coord.csv")
if my_file.is_file():
  os.system('rm predic_passagepieton/Tensorflow_coord.csv')
os.system("touch predic_passagepieton/Tensorflow_coord.csv")

with open('predic_passagepieton/Tensorflow_coord.csv', mode='a') as coordPassage:
  coordPassage = csv.writer(coordPassage, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
  coordPassage.writerow(['ID', 'longitude', 'latitude'])

  Id = 0
  for image_path in TEST_IMAGE_PATHS:
    print(image_path)
    image = Image.open(image_path)
    # the array based representation of the image will be used later in order to prepare the
    # result image with boxes and labels on it.
    image_np = load_image_into_numpy_array(image)
    # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
    image_np_expanded = np.expand_dims(image_np, axis=0)
    # Actual detection.
    output_dict = run_inference_for_single_image(image_np_expanded, detection_graph)

    # Visualization of the results of a detection.
    '''
    vis_util.visualize_boxes_and_labels_on_image_array(
        image_np,
        output_dict['detection_boxes'],
        output_dict['detection_classes'],
        output_dict['detection_scores'],
        category_index,
        instance_masks=output_dict.get('detection_masks'),
        use_normalized_coordinates=True,
        line_thickness=2)
    plt.figure(figsize=IMAGE_SIZE)
    plt.imshow(image_np)
    '''

    # This is the way I'm getting my coordinates
    boxes = output_dict['detection_boxes']
    # get all boxes from an array
    max_boxes_to_draw = boxes.shape[0]
    # get scores to get a threshold
    scores = output_dict['detection_scores']
    # this is set as a default but feel free to adjust it to your needs
    min_score_thresh=.5
    # iterate over all objects found
    

    idImg = image_path.split('/')
    idtrue = idImg[2]
    idtrue2 = idtrue.split('.')

    for i in range(min(max_boxes_to_draw, boxes.shape[0])):
      if scores is None or scores[i] > min_score_thresh:
        Id += 1
        # boxes[i] is the box which will be drawn
        ######class_name = category_index[output_dict['detection_classes'][i]]['name']
        ymin = boxes[i][0]*450
        xmin = boxes[i][1]*435
        ymax = boxes[i][2]*450
        xmax = boxes[i][3]*435
        
        larg = xmax-xmin
        long = ymax-ymin
        if larg*long > 2622:
          continue
        larg2 = larg/2
        long2 = long/2

        xpix = xmin + larg2
        ypix = ymin +	long2
        
        latitude, longitude = getLatLng("predic_passagepieton/tmp_til_tif/" + idtrue2[0] + ".tif", xpix, ypix)
        coordPassage.writerow([idtrue[0],latitude, longitude])
        

os.system("rm -r tmp_til_tif")
os.system("rm -r tmp_til_jpg")

